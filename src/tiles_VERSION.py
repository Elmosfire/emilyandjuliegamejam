import svgbuilder_VERSION as svgbuilder
from collections import namedtuple
from collections import deque

Connect = namedtuple("Connect", "N,E,S,W")

OPPOSITE = {"N":"S", "W":"E", "S":"N", "E":"W"}

class Tile:
    def __init__(self, conn: Connect, isHomeTile):
        self.connections = conn
        self.graphic_tile = self.generate_graphic_tile(isHomeTile)
        
        
    def generate_graphic_tile(self, isHomeTile):
        tdp = svgbuilder.TileDisplay()
        tdp.draw_road_structure(
            "N" in self.connections, 
            "E" in self.connections, 
            "S" in self.connections,
            "W" in self.connections,
            isHomeTile)
        return tdp
    
    def display(self, target):
        target.tiles = {(0,0): (self.graphic_tile, False)}
        
    @staticmethod
    def display_lst(lst, target):
        target.tiles = {((i%4)+(i//4),-(i%4)+(i//4)): (x.graphic_tile, False, []) for i,x in enumerate(lst)}
    
class TileSet:
    def __init__(self, deck, hometile):
        
        self.deck = deque(deck)
        self.tiles = {hometile: self.deck.pop(), (0,0): self.deck.popleft()}
        
        self.conn_f = {}
        self.conn_b = {}
        
    def on_click(self, x, y):
        if (x,y) == self.home_loc():
            return
        elif (x,y) in self.tiles:
            while (x,y) in self.tiles:
                if not self.undo():
                    break
        elif self.active_tile_valid((x,y)):
            for d in "NEWS":
                if self.get_border_tile(self.player_loc(), d)[1] == (x,y):
                    self.conn_f[self.player_loc()] = d
                    self.conn_b[(x,y)] = OPPOSITE[d]
            if len(self.deck) == 1:
                for d in "NEWS":
                    if self.get_border_tile(self.home_loc(), d)[1] == (x,y):
                        self.conn_b[self.home_loc()] = d
                        self.conn_f[(x,y)] = OPPOSITE[d]
            self.tiles[x,y] = self.deck.popleft()
         
    def get_border_tile(self, loc, dir_):
        x,y = loc
        if dir_ == "N":
            return self.tiles.get((x, y-1)), (x, y-1)
        elif dir_ == "S":
            return self.tiles.get((x, y+1)), (x, y+1)
        elif dir_ == "E":
            return self.tiles.get((x-1, y)), (x-1, y)
        elif dir_ == "W":
            return self.tiles.get((x+1, y)), (x+1, y)
        
        
    def tile_is_valid(self, tile, location, require_home_tile=False):
        if location in self.tiles:
            return False
        flag_home_tile = False
        for d in "NEWS":
            border_tile, border_tile_loc = self.get_border_tile(location, d)
            if border_tile is None:
                continue
            else:
                if (d in tile.connections) == (OPPOSITE[d] in border_tile.connections):
                    if border_tile_loc == self.player_loc():
                        if not (d in tile.connections):
                            return False
                    elif border_tile_loc == self.home_loc():
                        if (d in tile.connections):
                            flag_home_tile = True
                    continue
                else:
                    return False
        return (not require_home_tile) or flag_home_tile
    
    def active_tile_valid(self, location):
        if not self.deck:
            return False
        return self.tile_is_valid(self.deck[0], location, len(self.deck) == 1)
    
    def player_loc(self):
        return list(self.tiles)[-1]
    
    def home_loc(self):
        return list(self.tiles)[0]
    
    def get_highlights(self, location):
        f = self.conn_f.get(location, "")
        b = self.conn_b.get(location, "")
        
        if f:
            yield f"marker_{f}"
        if b:
            yield f"marker_{b}"
        if f and b:
            yield f"marker_center"
            
        
            
        if location == (self.player_loc() if self.deck else self.home_loc()):
            if b:
                yield f"car_{b}"
            else:
                newb = next(iter(self.tiles[location].connections))
                yield f"car_{newb}"
            
        
    
    def build_interface(self, target):
        tiles = {k: (v.graphic_tile, 0, list(self.get_highlights(k))) for k,v in self.tiles.items()}
        
        x,y = self.player_loc()
        
        for loc in [(x+1, y), (x-1, y), (x, y-1), (x, y+1)]:
            if self.active_tile_valid(loc):
                tiles[loc] = self.deck[0].graphic_tile, 1, []
                
            
        target.tiles = tiles
        
    def undo(self):
        if len(self.tiles) > 2:
            loc = self.player_loc()
            self.deck.appendleft(self.tiles[loc])
            del self.conn_b[loc]
            del self.tiles[loc]
            del self.conn_f[self.player_loc()]
            return True
        return False
        

                
    
        
        
def build_tileset_from_string(string):
    tiles, loc, _sol = string.split("_")
    tiles = tiles.split(",")
    return TileSet([Tile(x, False) for x in tiles[:-1]] + [Tile(tiles[-1], True)], tuple(int(x) for x in loc.split(",")))