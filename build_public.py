from pathlib import Path
import json

with open("version.json") as file:
    version = json.load(file)["version"]
    
version += 1

with open("version.json", "w") as file:
    json.dump({"version": version},file)

version_str = f"{version:06}"

for old_path in Path("./src").glob("*.*"):
    name = old_path.name
    newname = name.replace("VERSION", version_str)
    
    new_path = Path("./public") / newname
    
    with old_path.open("r") as f_in:
        with new_path.open("w") as f_out:
            f_out.write(f_in.read().replace("VERSION", version_str))