import tiles_000084 as tiles
from generate_level_000084 import generate
from lxml import etree
from js import document, window
from random import randrange
from functools import partial
from pyodide.ffi.wrappers import add_event_listener


query_string = window.location.search

try:
    options = {}
    query_string=query_string[1:]
    queries = query_string.split('&')
    for query in queries:
        name, value = query.split('=')
        print(name, value)
        options[name] = value
except ValueError:
    options = {}


def link_node_to_dev(element: etree.Element, div: str):
    document.getElementById(div).innerHTML = etree.tostring(element, pretty_print=True).decode("utf-8")

def reload(*_):
    window.location.href = window.location.href.split("?")[0]

def load_random_level_extreme(*_):
    loc = window.location.href.split("?")[0]
    window.location = loc + "?level=" + generate(24, 60)

def load_random_level_hard(*_):
    loc = window.location.href.split("?")[0]
    window.location = loc + "?level=" + generate(24, 30)
    
def load_random_level_medium(*_):
    loc = window.location.href.split("?")[0]
    window.location = loc + "?level=" + generate(12, 15)
    
def load_random_level_easy(*_):
    loc = window.location.href.split("?")[0]
    window.location = loc + "?level=" + generate(6, 4)
    
class Broker:
    def __init__(self, level):
        self.interface = tiles.svgbuilder.TileField()
        self.active_tile_interface = tiles.svgbuilder.TileField()
        self.all_tile_interface = tiles.svgbuilder.TileField()
        self.setup = tiles.build_tileset_from_string(level)
        
    def undo(self, *_):
        self.setup.undo()
        self.update_interface()
        
    def update_interface(self):
        self.setup.build_interface(self.interface)
        tiles.Tile.display_lst(list(self.setup.deck)[:1], self.active_tile_interface)
        tiles.Tile.display_lst(list(self.setup.deck), self.all_tile_interface)
    
        node, events = self.interface.build_node()
        link_node_to_dev(node, "gameboard")
        
        node3, _ = self.active_tile_interface.build_node()
        link_node_to_dev(node3, "active")
        
        node4, _ = self.all_tile_interface.build_node()
        link_node_to_dev(node4, "alltiles")
        
        for event in events:
            _, x, y = event.split("_")
            x,y = int(x), int(y)
            add_event_listener(document.getElementById(event), "click", partial(self.on_click, x, y))
            
        if not self.setup.deck:
            document.getElementById("youwon").hidden = False
            document.getElementById("otherlevel").innerHTML = document.getElementById("otherlevel").innerHTML + "  "
            
            
    def on_click(self, x, y, *_):
        self.setup.on_click(x, y)
        self.update_interface()
            


def main():
    document.getElementById("loading").innerHTML  = " "
    if 'level' in options:
        document.getElementById("game" ).hidden = False
        document.getElementById("rules").innerHTML  = " "
        document.getElementById("game" ).innerHTML += " "
        Broker(options["level"]).update_interface()
    else:
        document.getElementById("rules" ).hidden = False
        document.getElementById("game" ).innerHTML  = " "
        document.getElementById("rules").innerHTML += " "
        add_event_listener(document.getElementById("play"), "click", load_random_level_easy)
    add_event_listener(document.getElementById("otherlevel1"), "click", load_random_level_easy)
    add_event_listener(document.getElementById("otherlevel2"), "click", load_random_level_medium)
    add_event_listener(document.getElementById("otherlevel3"), "click", load_random_level_hard)
    add_event_listener(document.getElementById("otherlevel4"), "click", load_random_level_extreme)
    add_event_listener(document.getElementById("torules"), "click", reload)
        
    
    
    
        
        
    