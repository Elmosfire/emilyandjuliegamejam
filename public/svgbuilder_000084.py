from lxml.html import etree
from dataclasses import dataclass
from typing import List
from random import random, seed, choice
from copy import deepcopy
# seed(178)


DEPTHFACTOR = 0.5
HEIGHTFACTOR = 1


@dataclass(frozen=True)
class Point:
    x: float
    y: float
    z: float
    def proj_x(self):
        return self.x-self.y
    def proj_y(self):
        return (self.x+self.y)* DEPTHFACTOR - self.z * HEIGHTFACTOR
    def as_svg_point(self):
        return f"{self.proj_x()},{self.proj_y()}"
    def rotate90(self, x, y):
        return Point(self.y - y + x, -(self.x - x) + y, self.z)
        
    
@dataclass(frozen=True)
class PointList:
    pts: List[Point]
    def as_polygon(self, **kwargs):
        return etree.Element("polygon", dict(points=" ".join(x.as_svg_point() for x in self.pts), **kwargs))
    def as_polyline(self, **kwargs):
        return etree.Element("polyline", dict(points=" ".join(x.as_svg_point() for x in self.pts), **kwargs))
    def optimise_depth(self):
        return max(x.proj_y() for x in self.pts)
    def rotate90(self, x, y):
        return PointList([p.rotate90(x, y) for p in self.pts])
        

class TileDisplay():
    def __init__(self, sz=200):
        self.sz = sz
        self.polygons = []
        self.register_box(0, sz, 0, sz, -10, 0, "grey")
        
        
    def register_polygon(self, element, depth):
        self.polygons.append((depth, element))
        
        
    def rotate_to_orient(self, obj, orientation):
        cx, cy = self.sz//2, self.sz//2
        if orientation == "N":
            return obj
        elif orientation == "E":
            return obj.rotate90(cx, cy)
        elif orientation == "S":
            return obj.rotate90(cx, cy).rotate90(cx, cy)
        elif orientation == "W":
            return obj.rotate90(cx, cy).rotate90(cx, cy).rotate90(cx, cy)
        
        
    def draw_road(self, orientation, width=50):
        x1 = self.sz/2 - width/2
        x2 = self.sz/2 + width/2
        xc = self.sz/2
        y1 = 0
        y2 = self.sz/2 - width/2
        asphalt     = PointList([Point(x1, y1, 0), Point(x1, y2, 0), Point(x2, y2, 0), Point(x2, y1, 0)])
        left_line   = PointList([Point(x1, y1, 0), Point(x1, y2, 0)])
        right_line  = PointList([Point(x2, y1, 0), Point(x2, y2, 0)])
        center_line = PointList([Point(xc, y1 + width/2, 0), Point(xc, y2 - width/2, 0)])
        asphalt     = self.rotate_to_orient(asphalt    , orientation)
        left_line   = self.rotate_to_orient(left_line  , orientation)
        right_line  = self.rotate_to_orient(right_line , orientation)
        center_line = self.rotate_to_orient(center_line, orientation)
        self.register_polygon(asphalt.as_polygon(style=f"fill:lightgray;stroke:lightgray;stroke-width:1"), (1,0))
        self.register_polygon(asphalt.as_polygon(style=f"fill:#ffc87c;stroke:#ffc87c;stroke-width:1", hlid="marker_" + orientation), (1,1))
        self.register_polygon(left_line.as_polyline(style=f"stroke:black;stroke-width:1"), (1,2))
        self.register_polygon(right_line.as_polyline(style=f"stroke:black;stroke-width:1"), (1,2))
        self.register_polygon(center_line.as_polyline(style=f"stroke:black;stroke-width:1"), (1,2))
    
    
    def draw_road_center(self, width=50):
        x1 = self.sz/2 - width/2
        x2 = self.sz/2 + width/2
        y1 = self.sz/2 - width/2
        y2 = self.sz/2 + width/2
        asphalt = PointList([Point(x1, y1, 0), Point(x1, y2, 0), Point(x2, y2, 0), Point(x2, y1, 0)])
        self.register_polygon(asphalt.as_polygon(style=f"fill:lightgray;stroke:lightgray;stroke-width:1"), (1, 0))
        self.register_polygon(asphalt.as_polygon(style=f"fill:#ffc87c;stroke:#ffc87c;stroke-width:1", hlid="marker_center"), (1, 1))
        
        
    def draw_road_structure(self, do_N, do_E, do_S, do_W, isHomeTile=False):
        self.draw_road_center()
        self.draw_buildings(0, 0, isHomeTile)
        self.draw_buildings(0, 2, isHomeTile)
        self.draw_buildings(2, 0, isHomeTile)
        self.draw_buildings(2, 2, isHomeTile)
        if do_N:
            self.draw_road("N")
            self.draw_car("N")
        else:
            self.draw_buildings(1, 0, isHomeTile)
        if do_E:
            self.draw_road("E")
            self.draw_car("E")
        else:
            self.draw_buildings(0, 1, isHomeTile)
        if do_S:
            self.draw_road("S")
            self.draw_car("S") 
        else:
            self.draw_buildings(1, 2, isHomeTile)
        if do_W:
            self.draw_road("W")
            self.draw_car("W")
        else:
            self.draw_buildings(2, 1, isHomeTile)
        
            
    def draw_car(self, orientation, width=50, color="red"):
        center = 35
        step = 8
        
        xr = self.sz/2 - width/4 + step*0.7
        xc = self.sz/2 - width/4 - step*0.7
        
        y_front = center - 2.5*step
        y_w1f = center - 1.5*step
        y_w1b = center - 0.5*step
        y_w2f = center + 0.5*step
        y_w2b = center + 1.5*step
        y_back = center + 2.5*step  
        base = step*.5
        cap = step*1.5
        roof = step*2.5
        
        
        
        
        def register(pointlist, shift=0):
            points = self.rotate_to_orient(
                    PointList(pointlist),
                    orientation
                )
            depthloc_x = sum(p.x for p in points.pts)/len(points.pts)
            depthloc_y = sum(p.y for p in points.pts)/len(points.pts)
            self.register_polygon(
                points.as_polygon(
                    style=f"fill:{color};stroke:black;stroke-width:1",
                    hlid="car_" + orientation,
                ), 
                depth=(2, depthloc_x + depthloc_y + 1 + shift),
            )
        
        def car_sides(pts, shift=0):
            if orientation in "NW":
                register([Point(xr, y, z) for y,z in pts], step*2 + shift)
                register([Point(xc, y, z) for y,z in pts], -(step*2 + shift))
            else:
                register([Point(xr, y, z) for y,z in pts], -(step*2 + shift))
                register([Point(xc, y, z) for y,z in pts], step*2 + shift)
        
        def car_roof(pts):
            register([Point(xr, y, z) for y,z in pts] + [Point(xc, y, z) for y,z in reversed(pts)])
            
            
        car_sides([(y_w1f, 0), (y_w1b, 0), (y_w1b, step), (y_w1f, step)], step*3)
        car_sides([(y_w2f, 0), (y_w2b, 0), (y_w2b, step), (y_w2f, step)], step*3)
        
        car_sides([(y_front, base), (y_front, cap), (y_w1f, cap), (y_w1b, roof), (y_w2f, roof), (y_w2b, cap), (y_back, cap), (y_back, base)])
        
        car_roof([(y_front, base), (y_front, cap)])
        car_roof([(y_front, cap), (y_w1f, cap)])
        car_roof([(y_w1f, cap), (y_w1b, roof)])
        car_roof([(y_w1b, roof), (y_w2f, roof)])
        car_roof([(y_w2f, roof), (y_w2b, cap)])
        car_roof([(y_w2b, cap), (y_back, cap)])
        car_roof([(y_back, cap), (y_back, base)])
        
        
             
    
    def draw_buildings(self, xs, ys, isHomeTile=False):
        if (xs or ys) if isHomeTile else random() > 0.85:
            xshift = xs*self.sz/3
            yshift = ys*self.sz/3
            border = 4
            y_face = yshift + border
            y_back = yshift + self.sz/3 - border
            x_left = xshift + border
            x_right = xshift + self.sz/3 - border
            color=choice(["green", "darkgrey", "grey"])
            pool= random() > 0.75
            square = PointList([Point(x_right, y_face, 0), Point(x_left, y_face, 0), Point(x_left, y_back, 0), Point(x_right, y_back, 0)])
            self.register_polygon(square.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth=(2, xshift + yshift + 1))
            if pool:
                square2 = PointList(
                    [
                        Point(x_right - border*2.5, y_face + border*2.5, 0), 
                        Point(x_left + border*2.5, y_face + border*2.5, 0), 
                        Point(x_left + border*2.5, y_back - border*2.5, 0), 
                        Point(x_right - border*2.5, y_back - border*2.5, 0)
                    ]
                )
                self.register_polygon(square2.as_polygon(style=f"fill:blue;stroke:black;stroke-width:1"), depth=(2, xshift + yshift + 1))
        else:
            height = 15
            height_roof = 20 
            for xl in range(2):
                for yl in range(2):     
                    xshift = xs*self.sz/3 + self.sz/6*xl
                    yshift = ys*self.sz/3 + self.sz/6*yl
                    border = 8
                    
                    y_face = yshift + border
                    y_back = yshift + self.sz/6 - border
                    x_left = xshift + border
                    x_right = xshift + self.sz/6 - border
                    x_center = (x_left + x_right) / 2
                    color=choice(["yellow", "cyan", "white", "lime", None])
                    if isHomeTile:
                        if xs==0 and ys==0 and xl==1 and yl == 1:
                            color = "red"
                        else:
                            color = None
                    if color is None:
                        continue
                    
                    face = PointList([Point(x_left, y_face, 0), Point(x_left, y_face, height), Point(x_center, y_face, height_roof), Point(x_right, y_face, height), Point(x_right, y_face, 0)])
                    back = PointList([Point(x_left, y_back, 0), Point(x_left, y_back, height), Point(x_center, y_back, height_roof), Point(x_right, y_back, height), Point(x_right, y_back, 0)])
                    side = PointList([Point(x_right, y_face, 0), Point(x_right, y_face, height), Point(x_right, y_back, height), Point(x_right, y_back, 0)])
                    roof = PointList([Point(x_right, y_face, height), Point(x_center, y_face, height_roof), Point(x_center, y_back, height_roof), Point(x_right, y_back, height)])
                    roof2 = PointList([Point(x_left, y_face, height), Point(x_center, y_face, height_roof), Point(x_center, y_back, height_roof), Point(x_left, y_back, height)])
                    
                    self.register_polygon(face.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth=(2, xshift + yshift + 1))
                    self.register_polygon(back.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth=(2, xshift + yshift + 5))
                    self.register_polygon(side.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth=(2, xshift + yshift + 2))
                    self.register_polygon(roof.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth=(2, xshift + yshift + 3))
                    self.register_polygon(roof2.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth=(2, xshift + yshift + 3))
        
        
    def register_box(self, x1, x2, y1, y2, z1, z2, color="grey", depth=(0, 0)):
        mx = max(x1, x2)
        my = max(y1, y2)
        mz = max(z1, z2)
        
        xplane = PointList([Point(mx, y1, z1), Point(mx, y2, z1), Point(mx, y2, z2), Point(mx, y1, z2)])
        yplane = PointList([Point(x1, my, z1), Point(x2, my, z1), Point(x2, my, z2), Point(x1, my, z2)])
        zplane = PointList([Point(x1, y1, mz), Point(x1, y2, mz), Point(x2, y2, mz), Point(x2, y1, mz)])
        
        self.register_polygon(xplane.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth)
        self.register_polygon(yplane.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth)
        self.register_polygon(zplane.as_polygon(style=f"fill:{color};stroke:black;stroke-width:1"), depth)
        
        
    def build_svg_subgroup(self, xloc, yloc, id_="noid", hover=False, highlights=[]):
        translate = Point(self.sz*xloc, self.sz*yloc, 0).as_svg_point()
        if hover:
            rootnode = etree.Element("g", {"transform":f"translate({translate})", "class": "onlyhover", "id": id_})
        else:
            rootnode = etree.Element("g", {"transform":f"translate({translate})", "id": id_})
        for _, s in sorted(self.polygons.copy(), key=lambda item: item[0]):
            if hover:
                s = deepcopy(s)
            hl = s.attrib.get('hlid')
            if not hl or hl in highlights: 
                rootnode.append(s)
        return rootnode
            
            
class TileField:
    def __init__(self):
        self.tiles = {}
    def build_node(self):
        if not self.tiles:
            return etree.Element("span"), []
        shift = 200
        projections_x = [Point(shift*(x+1), shift*y, 0).proj_x() for x,y in self.tiles] + [Point(shift*x, shift*(y+1), 0).proj_x() for x,y in self.tiles]
        projections_y = [Point(shift*x, shift*y, 20).proj_y() for x,y in self.tiles] + [Point(shift*(x+1), shift*(y+1), -10).proj_y() for x,y in self.tiles]
        
        min_x_proj = min(projections_x)
        max_x_proj = max(projections_x)
        
        min_y_proj = min(projections_y) - 20
        max_y_proj = max(projections_y) + 5
        
        x_size = int(max_x_proj-min_x_proj)
        y_size = int(max_y_proj-min_y_proj)
        x_size = max(x_size, y_size*2)
        min_x_proj = int((min_x_proj+max_x_proj) - x_size)//2
        
        viewbox = f"{min_x_proj} {min_y_proj} {x_size} {y_size}"
        rootnode = etree.Element("svg", viewbox=viewbox)
        links = []
        for (x,y), (s, hover, hl) in sorted(self.tiles.items(), key=lambda item: item[0][0]+item[0][1]):
            rootnode.append(s.build_svg_subgroup(x, y, f"tile_{x}_{y}", hover, hl))
            links.append(f"tile_{x}_{y}")
        return rootnode, links
        
    
        
