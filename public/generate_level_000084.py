from random import choice, randrange
from functools import partial

DIRS = {
    "N":(1,0),
    "S":(-1,0),
    "E":(0,1),
    "W":(0,-1)
}

OPPOSITE = {"N":"S", "W":"E", "S":"N", "E":"W"}

def mv(loc, dr):
    x,y = loc
    dx, dy = DIRS[dr]
    return x+dx, y+dy
    
def connect(tiles, loc, dr):
    if loc in tiles:
        tiles[loc].add(dr)
    if mv(loc, dr) in tiles:
        tiles[mv(loc, dr)].add(OPPOSITE[dr])

def loss(loc, dr):
    x,y = mv(loc, dr)
    return x**2 + y**2

def generate(length, complexity):
    tiles = {(0,0):set()}
    
    x = 0
    y = 0
    
    sol = []
    for retry in range(100):
        try:
            for _ in range(length):
                valid = [z for z in "NESW" if mv((x,y), z) not in tiles]
                assert valid
                dr = min([choice(valid) for _ in range(3)], key=partial(loss, (x,y)))
                newloc = mv((x,y), dr)
                tiles[newloc] = set()
                connect(tiles, (x,y), dr)
                x,y = newloc
                sol.append(dr)
        except AssertionError as ex:
            flag = ex
            print("retry", retry)
            tiles = {(0,0):set()}
            x = 0
            y = 0
            sol = []
        else:
            break
    else:
        raise flag
        
    origtiles = tiles.copy()

            
    def add_random_connection():
        q = max([randrange(len(tiles)) for _ in range(2)])
        jmp = choice([-2, -1, 1, 2])
        nw = (q + jmp) % len(tiles)
        connect(tiles, list(tiles)[q], choice(list(list(origtiles.values())[nw]) + list(list(tiles.values())[nw]) + list("NEWS")*4))
        
    for _ in range(complexity):
        add_random_connection()
        
    lx, ly = list(tiles)[-1]
        
    return ",".join(''.join(v) for v in reversed(tiles.values())) + "_" + f"{ly},{lx}" + "_" + ''.join(reversed(sol))